package com.example.mems.Memologia.controller;

import com.example.mems.Memologia.entity.Test;
import com.example.mems.Memologia.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/memologia")
public class TestController {

    @Autowired
    TestService testService;

    @PostMapping("/add/{name}")
    public void addNewUser(@PathVariable String name) {
        testService.addNewUser(name);
    }

    @GetMapping("/show")
    public List<Test> showAllUser() {
        return testService.showAllUsers();
    }

    @GetMapping("/show/{id}")
    public Test showUserById(@PathVariable int id){
        return testService.showUserById(id);
    }

}
