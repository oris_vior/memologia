package com.example.mems.Memologia;

import java.io.File;

public class RenamePhotoMems {
    public static void main(String[] args) {
        File dir = new File("src/main/resources/mems");
        File[] arrFile = dir.listFiles();
        for (int i = 0; i < arrFile.length; i++) {
            if (arrFile[i].isFile()) {
                arrFile[i].renameTo(new File("src/main/resources/mems/" + i + ".jpeg"));
            }
        }
    }
}
