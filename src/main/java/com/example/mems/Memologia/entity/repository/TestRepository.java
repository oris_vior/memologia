package com.example.mems.Memologia.entity.repository;

import com.example.mems.Memologia.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TestRepository extends JpaRepository<Test, Integer> {
    Test findTestById(Integer id);

    @Override
    List<Test> findAll();
}
