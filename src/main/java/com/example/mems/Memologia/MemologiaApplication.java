package com.example.mems.Memologia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

@SpringBootApplication
public class MemologiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemologiaApplication.class, args);
	}

}
