package com.example.mems.Memologia.service;

import com.example.mems.Memologia.entity.Test;
import com.example.mems.Memologia.entity.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TestService {
    @Autowired
    TestRepository testRepository;

    public void addNewUser(String name) {
        testRepository.save(new Test(name));
    }

    public List<Test> showAllUsers() {
        return testRepository.findAll();
    }

    public Test showUserById(int id) {
        return testRepository.findTestById(id);
    }
}
